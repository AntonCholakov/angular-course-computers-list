import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {ComputersComponent} from './computers/computers.component';
import {HttpClientModule} from '@angular/common/http';
import {ComputersService} from './computers/services/computers.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ComputersComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    ComputersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
