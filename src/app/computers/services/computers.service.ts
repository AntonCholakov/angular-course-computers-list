import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Computer} from '../models/computer.model';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ComputersService {

  constructor(private httpClient: HttpClient) {
  }

  public getAll(): Observable<Computer[]> {
    return this.httpClient.get<Computer[]>('http://localhost:3000/computers');
  }

  public getById(id: string): Observable<Computer> {
    return this.httpClient.get<Computer>('http://localhost:3000/computers/' + id);
  }

  public delete(id: string): Observable<any> {
    return this.httpClient.delete<Observable<any>>('http://localhost:3000/computers/' + id);
  }

  public create(computer: Computer): Observable<Computer> {
    return this.httpClient.post('http://localhost:3000/computers', computer);
  }

  public update(computer: Computer): Observable<Computer> {
    return this.httpClient.put('http://localhost:3000/computers/' + computer.id, computer);
  }

}
