import {Component, OnInit} from '@angular/core';
import {Computer} from './models/computer.model';
import {ComputersService} from './services/computers.service';

@Component({
  selector: 'app-computers',
  templateUrl: './computers.component.html',
  styleUrls: ['./computers.component.scss']
})
export class ComputersComponent implements OnInit {

  computers: Computer[];

  constructor(private computersService: ComputersService) {
  }

  ngOnInit() {
    this.computersService.getAll().subscribe((computers) => {
      this.computers = computers;
    });
  }

}
