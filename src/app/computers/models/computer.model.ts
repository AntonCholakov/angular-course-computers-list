export class Computer {
  id: number;
  manufacturer: string;
  brand: string;
  year: number;
  ram: number;
  video: number;
}
